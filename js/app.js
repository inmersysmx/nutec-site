
const ENTIRE_SCENE = 0, BLOOM_SCENE = 1;

const bloomLayer = new THREE.Layers();
bloomLayer.set(BLOOM_SCENE);

let stats, mixer, clock;

let camera, controls, scene, renderer, webgl_container;

let video, cubeMesh2, texture;

let meshLogo1, meshLogo2, meshLogo3;

let materialOne, materialTwo, materialThree, materialCircles, materialCircles1, materialCircles2, materialCircles3, materialCircles4;

let dirLight1, dirLight2;
let ambientLight;

let finalComposer, bloomComposer, bloomPass;

let totalCounter = 0;

const textureLoader = new THREE.TextureLoader();

const darkMaterial = new THREE.MeshBasicMaterial({ color: "black" });

const materials = {};

const gltfloader = new THREE.GLTFLoader();

const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

const paramsBloom = {
    exposure: 1,
    bloomStrength: 1.3,
    bloomThreshold: 0,
    bloomRadius: 0
};

const paramsMaterial = {
    roughness: 0,
    metalness: 0,
    emissiveIntensity: 0,
    aoMapIntensity: 0,
    normalScale: 0,
    color: 0xFFFFFF,
    emissive: 0xFFFFFF,
};

const paramsMaterialTwo = {
    roughness: 0,
    metalness: 0,
    emissiveIntensity: 0,
    aoMapIntensity: 0,
    normalScale: 0,
    color: 0xFFFFFF,
    emissive: 0xFFFFFF,
};

const paramsMaterialCircle = {
    roughness: 1,
    metalness: 0,
    emissiveIntensity: 0.45,
    aoMapIntensity: 0,
    normalScale: 0,
    color: 0x234b91,
    emissive: 0xFFFFFF,
};

const paramsMaterialCircle1 = {
    roughness: 1,
    metalness: 0,
    emissiveIntensity: 0.47,
    aoMapIntensity: 0,
    normalScale: 0,
    color: 0xFFCE,
    emissive: 0xFFFFFF,
};

const paramsMaterialCircle2 = {
    roughness: 1,
    metalness: 0,
    emissiveIntensity: 0.41,
    aoMapIntensity: 0,
    normalScale: 0,
    color: 0x000000,
    emissive: 0x2e2eb1,
};

const paramsMaterialCircle3 = {
    roughness: 1,
    metalness: 0,
    emissiveIntensity: 0.5,
    aoMapIntensity: 0,
    normalScale: 0,
    color: 0xca27f,
    emissive: 0xFFFFFF,
};

const paramsMaterialCircle4 = {
    roughness: 1,
    metalness: 0,
    emissiveIntensity: 0.27,
    aoMapIntensity: 0,
    normalScale: 0,
    color: 0xb175f,
    emissive: 0xFFFFFF,
};


const luzParams = {
    intensity: 0,
    color: 0xFFFFFF,
};

const luz2Params = {
    intensity: 0,
    color: 0xFFFFFF,
};

const luz3Params = {
    intensity: 0,
    color: 0xFFFFFF,
};

window.onload = function () {
    init();
    initModel();
    // initVideo();
    initModelCircles();
    initLogos();
    // animate();


    document.getElementById("id-next-img").addEventListener("click", onNextImage);

    document.getElementById('video').addEventListener('ended', function (event) {
        document.getElementById('video').currentTime = 0;
        onShowPlayButton();
        onShowModal();
    });

    document.getElementById("id-videoOne-btn").addEventListener("click", onVideoOneClicked);
    document.getElementById("id-videoTwo-btn").addEventListener("click", onVideoTwoClicked);
    document.getElementById("id-videoThree-btn").addEventListener("click", onVideoThreeClicked);
};

function init() {

    scene = new THREE.Scene();

    webgl_container = document.getElementById("id-webgl-container");

    renderer = new THREE.WebGLRenderer({ canvas: webgl_container, antialias: true });
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
    renderer.setSize(sizes.width, sizes.height);

    clock = new THREE.Clock();

    camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 1100);
    camera.position.set(-200, 0, 0);

    // controls
    controls = new THREE.OrbitControls(camera, webgl_container);
    controls.enableDamping = true;
    controls.enableZoom = false;
    controls.dampingFactor = 0.05;
    controls.autoRotate = true;
    controls.autoRotateSpeed = 0.2;
    controls.enabled = true;

    controls.minDistance = 100;
    controls.maxDistance = 500;

    controls.maxPolarAngle = Math.PI / 2;
    controls.minPolarAngle = Math.PI / 2.15;

    controls.minAzimuthAngle = - Math.PI / 1.5;
    controls.maxAzimuthAngle = - Math.PI / 3.5;

    /** BLOOM **/
    const renderScene = new THREE.RenderPass(scene, camera);
    bloomPass = new THREE.UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), 1.5, 0.4, 0.85);
    bloomPass.threshold = paramsBloom.bloomThreshold;
    bloomPass.strength = paramsBloom.bloomStrength;
    bloomPass.radius = paramsBloom.bloomRadius;

    bloomComposer = new THREE.EffectComposer(renderer);
    bloomComposer.renderToScreen = false;
    bloomComposer.addPass(renderScene);
    bloomComposer.addPass(bloomPass);

    const finalPass = new THREE.ShaderPass(
        new THREE.ShaderMaterial({
            uniforms: {
                baseTexture: { value: null },
                bloomTexture: { value: bloomComposer.renderTarget2.texture }
            },
            vertexShader: document.getElementById('vertexshader').textContent,
            fragmentShader: document.getElementById('fragmentshader').textContent,
            defines: {}
        }), "baseTexture"
    );
    finalPass.needsSwap = true;

    finalComposer = new THREE.EffectComposer(renderer);
    finalComposer.addPass(renderScene);
    finalComposer.addPass(finalPass);

    // lights
    dirLight1 = new THREE.DirectionalLight(0xffffff);
    dirLight1.position.set(1, 1, 1);
    scene.add(dirLight1);

    dirLight2 = new THREE.DirectionalLight(0xffffff);
    dirLight2.position.set(- 1, - 1, - 1);
    scene.add(dirLight2);

    ambientLight = new THREE.AmbientLight(0xffffff);
    scene.add(ambientLight);

    window.addEventListener('resize', onWindowResize);

}

function onWindowResize() {

    // update sizes
    sizes.width = window.innerWidth;
    sizes.height = window.innerHeight;

    camera.aspect = sizes.width / sizes.height;
    camera.updateProjectionMatrix();
    renderer.setSize(sizes.width, sizes.height);
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

    bloomComposer.setSize(sizes.width, sizes.height);
    finalComposer.setSize(sizes.width, sizes.height);

}

function animate() {

    requestAnimationFrame(animate);

    if (cubeMesh2) {
        cubeMesh2.quaternion.copy(camera.quaternion);
    }

    if (meshLogo1) {
        meshLogo1.quaternion.copy(camera.quaternion);
    }

    const delta = clock.getDelta();
    if (mixer) {
        mixer.update(delta);
    }

    controls.update();
    renderBloom(true);
    finalComposer.render();

}

function render() {
    renderer.render(scene, camera);
}

function initModel() {
    gltfloader.load(
        './model/scene.gltf',
        function (gltf) {

            // gltf.scene.traverse((child) => {

            //     if (!child.isMesh) return;

            //     var prevMaterial = child.material;

            //     child.material = new THREE.MeshBasicMaterial();

            //     THREE.MeshBasicMaterial.prototype.copy.call(child.material, prevMaterial);

            // });


            gltf.scene.scale.set(10, 10, 10);
            gltf.scene.position.set(0, -50, 3);

            materialOne = gltf.scene.getObjectByName("defaultMaterial", true).material;
            materialTwo = gltf.scene.getObjectByName("defaultMaterial_1", true).material;
            materialThree = gltf.scene.getObjectByName("defaultMaterial_2", true).material;

            textureLoader.load('./model/textures/TXT_Body_M.png', function (texture) {
                gltf.scene.getObjectByName("defaultMaterial_1", true).material.metalnessMap = texture;
                addLoader();
                return texture;
            });
            textureLoader.load('./model/textures/TXT_Body_AL.png', function (texture) {
                gltf.scene.getObjectByName("defaultMaterial_1", true).material.map = texture;
                addLoader();
                return texture;
            });
            // textureLoader.load('./model/textures/TXT_Body_NRM.png', function (texture) {
            //     gltf.scene.getObjectByName("defaultMaterial_1", true).material.normalMap = texture;
            //     gltf.scene.getObjectByName("defaultMaterial_1", true).material.normalScale = new THREE.Vector2(1.5, 1.5);
            //     addLoader();
            //     return texture;
            // });

            textureLoader.load('./model/textures/TXT_Body_R.png', function (texture) {
                gltf.scene.getObjectByName("defaultMaterial_1", true).material.roughnessMap = texture;
                addLoader();
                return texture;
            });
            console.log("MATERIAL: ", gltf.scene.getObjectByName("defaultMaterial", true)); //"defaultMaterial_1"

            scene.add(gltf.scene);

            addLoader();

        },
        // called while loading is progressing
        function (xhr) {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded');

        },
        // called when loading has errors
        function (error) {
            console.log('An error happened: ', error);
        }
    );
}

function initModelCircles() {
    const _gltfLoader = new THREE.GLTFLoader();
    const dracoLoader = new THREE.DRACOLoader();
    dracoLoader.setDecoderPath('./js/draco/');
    _gltfLoader.setDRACOLoader(dracoLoader);
    _gltfLoader.load('./model/GLB_PrimaryIonDrive_draco.gltf', function (gltf) {

        const model = gltf.scene;
        model.scale.set(100, 100, 100);
        model.position.set(300, 0, 0);

        model.getObjectByName("geo1_HoloFillDark_0", true).visible = false;
        model.getObjectByName("geo1_constant1_0", true).visible = false;
        model.getObjectByName("circle_constant1_0", true).material = new THREE.MeshStandardMaterial(paramsMaterialCircle);
        materialCircles = model.getObjectByName("circle_constant1_0", true).material;
        model.getObjectByName("circle1_constant2_0", true).material = new THREE.MeshStandardMaterial(paramsMaterialCircle1);
        materialCircles1 = model.getObjectByName("circle1_constant2_0", true).material;
        // model.getObjectByName("circle_HoloFillDark_0", true).material = new THREE.MeshStandardMaterial(paramsMaterialCircle2);
        materialCircles2 = model.getObjectByName("circle_HoloFillDark_0", true).material;
        model.getObjectByName("circle2", true).material = new THREE.MeshStandardMaterial(paramsMaterialCircle3);
        materialCircles3 = model.getObjectByName("circle2", true).material;
        model.getObjectByName("circle2_constant2_0", true).material = new THREE.MeshStandardMaterial(paramsMaterialCircle4);
        materialCircles4 = model.getObjectByName("circle2_constant2_0", true).material;
        // model.getObjectByName("circle_constant1_0", true).material.visible = false;
        model.getObjectByName("circle_constant1_0", true).layers.enable(BLOOM_SCENE);
        model.getObjectByName("circle1_constant2_0", true).layers.enable(BLOOM_SCENE);
        model.getObjectByName("circle_HoloFillDark_0", true).layers.enable(BLOOM_SCENE);//
        model.getObjectByName("circle2", true).layers.enable(BLOOM_SCENE);
        model.getObjectByName("circle2_constant2_0", true).layers.enable(BLOOM_SCENE);
        console.log({ materialCircles });

        console.log("BLOOM: ", model);
        scene.add(model);

        mixer = new THREE.AnimationMixer(model);
        const clip = gltf.animations[0];
        mixer.clipAction(clip.optimize()).play();

        addLoader();

    });
}

function initLogos() {

    const cube = new THREE.BoxGeometry(100, 16.73, 1);
    const material = new THREE.MeshBasicMaterial({ map: textureLoader.load('./LOGO_NUTEC_FOOTER_NEGATIVO.png'), transparent: true, opacity: 1.0, color: 0xFFFFFF });
    meshLogo1 = new THREE.Mesh(cube, material);
    meshLogo1.position.set(20, 80, 10);
    scene.add(meshLogo1);

}

function initVideo() {
    video = document.getElementById('video');

    texture = new THREE.VideoTexture(video);
    texture.format = THREE.RGBAFormat;

    const cube2 = new THREE.BoxGeometry(175, 98.45, 1);
    const material22 = new THREE.MeshBasicMaterial({ map: texture, transparent: true, opacity: 1.0 });
    cubeMesh2 = new THREE.Mesh(cube2, material22);
    cubeMesh2.position.set(0, 12, 0);
    scene.add(cubeMesh2);
}

function onNextImage() {
    console.log("PLAY");

    if (!video) {
        initVideo();
    }
    video.loop = false;
    video.muted = false;
    video.currentTime = 0;
    video.play();
    onHidePlayButton();
    onHideModal();
}

function initGUI() {

    stats = new Stats();
    document.body.appendChild(stats.dom);

    const gui = new GUI();

    const pisoFolder = gui.addFolder('Piso')

    pisoFolder.add(paramsMaterial, 'roughness', 0.1, 1).onChange(function (value) {

        materialOne.roughness = Number(value);

    });

    pisoFolder.add(paramsMaterial, 'metalness', 0.0, 1.0).onChange(function (value) {

        materialOne.metalness = Number(value);

    });

    pisoFolder.add(paramsMaterial, 'emissiveIntensity', 0.0, 1.0).onChange(function (value) {

        materialOne.emissiveIntensity = Number(value);

    });

    pisoFolder.add(paramsMaterial, 'aoMapIntensity', 0.0, 1.0).onChange(function (value) {

        materialOne.aoMapIntensity = Number(value);

    });

    pisoFolder.add(paramsMaterial, 'normalScale', 0.0, 1.0).onChange(function (value) {

        materialOne.normalScale = new THREE.Vector2(Number(value), Number(value));//  Number(value);

    });

    pisoFolder.addColor(paramsMaterial, 'color').onChange(function (colorValue) {
        materialOne.color.set(colorValue);
    });

    pisoFolder.addColor(paramsMaterial, 'emissive').onChange(function (colorValue) {
        materialOne.emissive.set(colorValue);
    });


    const paredFolder = gui.addFolder('Pared y Techo')

    paredFolder.add(paramsMaterialTwo, 'roughness', 0.1, 1).onChange(function (value) {

        materialTwo.roughness = Number(value);

    });

    paredFolder.add(paramsMaterialTwo, 'metalness', 0.0, 1.0).onChange(function (value) {

        materialTwo.metalness = Number(value);

    });

    paredFolder.add(paramsMaterialTwo, 'emissiveIntensity', 0.0, 1.0).onChange(function (value) {

        materialTwo.emissiveIntensity = Number(value);

    });

    paredFolder.add(paramsMaterialTwo, 'aoMapIntensity', 0.0, 1.0).onChange(function (value) {

        materialTwo.aoMapIntensity = Number(value);

    });

    paredFolder.add(paramsMaterialTwo, 'normalScale', 0.0, 1.0).onChange(function (value) {

        materialTwo.normalScale = new THREE.Vector2(Number(value), Number(value));//  Number(value);

    });

    paredFolder.addColor(paramsMaterialTwo, 'color').onChange(function (colorValue) {
        materialTwo.color.set(colorValue);
    });

    paredFolder.addColor(paramsMaterialTwo, 'emissive').onChange(function (colorValue) {
        materialTwo.emissive.set(colorValue);
    });

    const circleFolder = gui.addFolder('Circulo')

    circleFolder.add(paramsMaterialCircle, 'roughness', 0.1, 1).onChange(function (value) {
        materialCircles.roughness = Number(value);
    });

    circleFolder.add(paramsMaterialCircle, 'metalness', 0.0, 1.0).onChange(function (value) {
        materialCircles.metalness = Number(value);
    });

    circleFolder.add(paramsMaterialCircle, 'emissiveIntensity', 0.0, 1.0).onChange(function (value) {
        materialCircles.emissiveIntensity = Number(value);
    });

    circleFolder.addColor(paramsMaterialCircle, 'color').onChange(function (colorValue) {
        materialCircles.color.set(colorValue);
    });

    circleFolder.addColor(paramsMaterialCircle, 'emissive').onChange(function (colorValue) {
        materialCircles.emissive.set(colorValue);
    });

    const circleFolder1 = gui.addFolder('Circulo2')

    circleFolder1.add(paramsMaterialCircle1, 'roughness', 0.1, 1).onChange(function (value) {
        materialCircles1.roughness = Number(value);
    });

    circleFolder1.add(paramsMaterialCircle1, 'metalness', 0.0, 1.0).onChange(function (value) {
        materialCircles1.metalness = Number(value);
    });

    circleFolder1.add(paramsMaterialCircle1, 'emissiveIntensity', 0.0, 1.0).onChange(function (value) {
        materialCircles1.emissiveIntensity = Number(value);
    });

    circleFolder1.addColor(paramsMaterialCircle1, 'color').onChange(function (colorValue) {
        materialCircles1.color.set(colorValue);
    });

    circleFolder1.addColor(paramsMaterialCircle1, 'emissive').onChange(function (colorValue) {
        materialCircles1.emissive.set(colorValue);
    });

    const circleFolder2 = gui.addFolder('Circulo3')

    circleFolder2.add(paramsMaterialCircle2, 'roughness', 0.1, 1).onChange(function (value) {
        materialCircles2.roughness = Number(value);
    });

    circleFolder2.add(paramsMaterialCircle2, 'metalness', 0.0, 1.0).onChange(function (value) {
        materialCircles2.metalness = Number(value);
    });

    circleFolder2.add(paramsMaterialCircle2, 'emissiveIntensity', 0.0, 1.0).onChange(function (value) {
        materialCircles2.emissiveIntensity = Number(value);
    });

    circleFolder2.addColor(paramsMaterialCircle2, 'color').onChange(function (colorValue) {
        materialCircles2.color.set(colorValue);
    });

    circleFolder2.addColor(paramsMaterialCircle2, 'emissive').onChange(function (colorValue) {
        materialCircles2.emissive.set(colorValue);
    });

    const circleFolder3 = gui.addFolder('Circulo4')

    circleFolder3.add(paramsMaterialCircle3, 'roughness', 0.1, 1).onChange(function (value) {
        materialCircles3.roughness = Number(value);
    });

    circleFolder3.add(paramsMaterialCircle3, 'metalness', 0.0, 1.0).onChange(function (value) {
        materialCircles3.metalness = Number(value);
    });

    circleFolder3.add(paramsMaterialCircle3, 'emissiveIntensity', 0.0, 1.0).onChange(function (value) {
        materialCircles3.emissiveIntensity = Number(value);
    });

    circleFolder3.addColor(paramsMaterialCircle3, 'color').onChange(function (colorValue) {
        materialCircles3.color.set(colorValue);
    });

    circleFolder3.addColor(paramsMaterialCircle3, 'emissive').onChange(function (colorValue) {
        materialCircles3.emissive.set(colorValue);
    });

    const circleFolder4 = gui.addFolder('Circulo5')

    circleFolder4.add(paramsMaterialCircle4, 'roughness', 0.1, 1).onChange(function (value) {
        materialCircles4.roughness = Number(value);
    });

    circleFolder4.add(paramsMaterialCircle4, 'metalness', 0.0, 1.0).onChange(function (value) {
        materialCircles4.metalness = Number(value);
    });

    circleFolder4.add(paramsMaterialCircle4, 'emissiveIntensity', 0.0, 1.0).onChange(function (value) {
        materialCircles4.emissiveIntensity = Number(value);
    });

    circleFolder4.addColor(paramsMaterialCircle4, 'color').onChange(function (colorValue) {
        materialCircles4.color.set(colorValue);
    });

    circleFolder4.addColor(paramsMaterialCircle4, 'emissive').onChange(function (colorValue) {
        materialCircles4.emissive.set(colorValue);
    });

    const luzFolder = gui.addFolder('Luz 1')

    luzFolder.add(luzParams, 'intensity', 0.1, 10).onChange(function (value) {

        dirLight1.intensity = Number(value);

    });

    luzFolder.addColor(luzParams, 'color').onChange(function (colorValue) {
        dirLight1.color.set(colorValue);
    });

    const luz2Folder = gui.addFolder('Luz 2')

    luz2Folder.add(luz2Params, 'intensity', 0.1, 10).onChange(function (value) {

        dirLight2.intensity = Number(value);

    });

    luz2Folder.addColor(luz2Params, 'color').onChange(function (colorValue) {
        dirLight2.color.set(colorValue);
    });

    const luz3Folder = gui.addFolder('Luz ambiente')

    luz3Folder.add(luz3Params, 'intensity', 0.1, 10).onChange(function (value) {

        ambientLight.intensity = Number(value);

    });

    luz3Folder.addColor(luz3Params, 'color').onChange(function (colorValue) {
        ambientLight.color.set(colorValue);
    });

    const folder = gui.addFolder('Bloom Parameters');

    folder.add(paramsBloom, 'exposure', 0.1, 2).onChange(function (value) {

        renderer.toneMappingExposure = Math.pow(value, 4.0);
        render();

    });

    folder.add(paramsBloom, 'bloomThreshold', 0.0, 1.0).onChange(function (value) {

        bloomPass.threshold = Number(value);
        render();

    });

    folder.add(paramsBloom, 'bloomStrength', 0.0, 10.0).onChange(function (value) {

        bloomPass.strength = Number(value);
        render();

    });

    folder.add(paramsBloom, 'bloomRadius', 0.0, 1.0).step(0.01).onChange(function (value) {

        bloomPass.radius = Number(value);
        render();

    });

    gui.close();

}

function renderBloom(mask) {

    if (mask === true) {

        scene.traverse(darkenNonBloomed);
        bloomComposer.render();
        scene.traverse(restoreMaterial);

    } else {

        camera.layers.set(BLOOM_SCENE);
        bloomComposer.render();
        camera.layers.set(ENTIRE_SCENE);

    }

}

function darkenNonBloomed(obj) {

    if (obj.isMesh && bloomLayer.test(obj.layers) === false) {

        materials[obj.uuid] = obj.material;
        obj.material = darkMaterial;

    }

}

function restoreMaterial(obj) {

    if (materials[obj.uuid]) {

        obj.material = materials[obj.uuid];
        delete materials[obj.uuid];

    }

}

function onActiveExperience() {
    const experience_container = document.getElementById("id-webgl-container");
    experience_container.style.display = "flex";
}

function onActiveModal() {
    const experience_container = document.getElementById("id-modal-container");
    experience_container.style.display = "flex";
}

function onVideoOneClicked() {
    onSetVideoElement("vKz0qP2Lxa0");
}

function onVideoTwoClicked() {
    onSetVideoElement("HkwRjpyh1XI");
}

function onVideoThreeClicked() {
    onSetVideoElement("4tpyJwQ-nh4");
}

function onSetVideoElement(youtubeIb) {
    console.log("onSetVideoElement");
    const iframe_element = document.getElementById("id-iframe-video");
    iframe_element.src = "https://www.youtube.com/embed/" + youtubeIb;
}

function onHidePlayButton() {
    document.getElementById("id-next-img").style.display = 'none';
}

function onShowPlayButton() {
    document.getElementById("id-next-img").style.display = 'block';
}

function onHideModal() {
    document.getElementById("id-iframe-video").src = "";
    document.getElementById("id-modal-container").style.display = 'none';
}

function onShowModal() {
    document.getElementById("id-iframe-video").src = "https://www.youtube.com/embed/vKz0qP2Lxa0";
    document.getElementById("id-modal-container").style.display = 'flex';
}

function addLoader() {
    totalCounter++;
    if (totalCounter >= 5) {
        console.log("LOAD FINISH");
        document.getElementById("id-container-loading").style.display = 'none';
        animate();
    }
}
